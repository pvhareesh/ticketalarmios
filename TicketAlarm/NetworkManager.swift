//
//  NetworkManager.swift
//  TicketAlarm
//
//  Created by Hareesh on 8/6/20.
//  Copyright © 2020 Hareesh. All rights reserved.
//

import Combine
import SwiftUI

class NetworkManager: ObservableObject, Identifiable {
    
    let server_Url = "http://192.168.1.12:8199/tktNtfPrm"
    
    var didChange = PassthroughSubject<NetworkManager, Never>()
    @Published var movies = [UpcomingMovie](){
        didSet {
            didChange.send(self)
        }
    }
    @Published var locations = [String](){
        didSet {
            didChange.send(self)
        }
    }
    
    
    init() {
        guard let url = URL(string: server_Url + "/getWeekendMovies") else {return}
        URLSession.shared.dataTask(with: url){ (data, response, error) in
            guard let data = data else { return }
            let movies: [UpcomingMovie] = try! JSONDecoder().decode([UpcomingMovie].self, from: data)
            DispatchQueue.main.async {
                self.movies = movies
                print(movies)
            }
        }.resume()
        
        guard let locUrl = URL(string: server_Url + "/getLocations") else {return}
        URLSession.shared.dataTask(with: locUrl){ (data, response, error) in
            guard let data = data else { return }
            let locs: [String] = try! JSONDecoder().decode([String].self, from: data)
            DispatchQueue.main.async {
                self.locations = locs
                print(locs)
            }
        }.resume()
    }
}

struct UpcomingMovie: Decodable {
    let id = UUID()
    let movieId: String
    let title: String
    var releaseDate: String
    var siteName: String
    var bookingLink: String
}

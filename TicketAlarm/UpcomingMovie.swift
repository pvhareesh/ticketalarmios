//
//  UpcomingMovie.swift
//  TicketAlarm
//
//  Created by Hareesh on 8/7/20.
//  Copyright © 2020 Hareesh. All rights reserved.
//

struct UpcomingMovie: Decodable {
    var uid = UUID()
    var movieId: String
    var title: String
    var releaseDate: String
    var siteName: String
    var bookingLink: String
}

struct MovieList: Decodable {
    var results: [UpcomingMovie]
}

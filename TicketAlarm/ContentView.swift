//
//  ContentView.swift
//  TicketAlarm
//
//  Created by Hareesh on 8/4/20.
//  Copyright © 2020 Hareesh. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var network_controller = NetworkManager()
    
    @State private var selectedMovie = 0
    @State private var selectedLoc = 0

    @State var selectedDate = Date()
    @State var expand = false
    var body: some View {
        VStack {
            NavigationView {
                Form {
                    Section {
                        HStack {
                            Spacer()
                            Image("ic_launcher")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 250.0, height: 250.0)
                            Spacer()
                        }
                        Picker(selection: $selectedMovie, label: Text("SelectMovie").foregroundColor(.blue)) {
                            ForEach(0 ..< self.network_controller.movies.count) { index in
                                Text("\(self.network_controller.movies[index].title)").tag(index)
                            }
                        }.id(UUID())
                        Picker(selection: $selectedLoc, label: Text("Select Area").foregroundColor(.blue)) {
                            ForEach(0 ..< self.network_controller.locations.count) { index in
                                Text("\(self.network_controller.locations[index])").tag(index)
                            }
                        }.id(UUID())
                        DatePicker(selection: $selectedDate, displayedComponents: .date) {
                            Text("Select Date").foregroundColor(.blue)
                        }
                        HStack {
                            Spacer()
                            Button(action: {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyyMMdd"
                                formatter.timeZone = TimeZone(abbreviation: "IST")
                                var formattedSelectedDate = formatter.string(from: self.selectedDate)
                                createNotifier(mobile:"919035025085", movieId:self.network_controller.movies[self.selectedMovie].movieId, area:self.network_controller.locations[self.selectedLoc], date:formattedSelectedDate)
                            }, label: {
                                Text("CREATE ALARM")
                                    .padding()
                                    .border(Color.blue)
                            }).padding()
                            Spacer()
                        }
                        Text("Please select Movie of your choice. We will send you an alarm when tickets are available").foregroundColor(.gray).padding()
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

let server_Url = "http://192.168.1.6:8199/tktNtfPrm"

func createNotifier(mobile:String, movieId:String, area:String, date:String) {
    
    
    guard let endpointUrl = URL(string: server_Url + "/addNotifier") else {
        return
    }
    
    //Make JSON to send to send to server
    var json = ["mId" : movieId, "m" : mobile, "dt" : date,"areaInCity" : area, "city" : "Hyd"]
    
    do {
        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        var request = URLRequest(url: endpointUrl)
        request.httpMethod = "POST"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request)  { (data, response, error) in
            if let error = error {
                print("error:", error)
                return
            }
            
            do {
                guard let data = data else { return }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else { return }
                print("json:", json)
            } catch {
                print("error:", error)
            }
        }
        task.resume()
    }catch{
    }
}
